import fs from "fs";
import https from "https";
import { exit } from "process";
import { EMAIL_PATH, TEXT_PATH, INDEX_FILE } from "./config";

let startIndex =
  process.argv.length > 2 && Number.isInteger(Number.parseInt(process.argv[2]))
    ? Number.parseInt(process.argv[2])
    : 0;
let batchSize =
  process.argv.length > 3 && Number.isInteger(Number.parseInt(process.argv[3]))
    ? Number.parseInt(process.argv[3])
    : Number.MAX_SAFE_INTEGER;

interface IndexCard {
  index?: number;
  downloadPath?: string;
  subject: string;
  documentClass: string;
  pdfLink: string;
  originalLink: null;
  docDate: string;
  postedDate: string;
  from: string;
  to: string;
  messageNumber: "" | null;
  caseNumber: string;
  autnID: string;
  cadreRefID: string;
}

class Scrape {
  indexCards: IndexCard[] = [];

  constructor() {}

  async scrape() {
    if (!fs.existsSync(EMAIL_PATH)) {
      fs.mkdirSync(EMAIL_PATH);
    }

    await this.getIndex();
    await this.downloadBatch(startIndex, batchSize);
  }

  private async downloadBatch(startIndex: number, batchSize: number) {
    console.log(
      `batch downloading ${batchSize} starting at index ${startIndex}`
    );

    const options = {
      hostname: "foia.state.gov",
      port: 443,
      method: "GET",
      headers: {
        Accept: "*/*",
        "Accept-Encoding": "gzip, deflate, br",
        Host: "foia.state.gov",
      },
    };

    const len = startIndex + batchSize;

    for (let i = startIndex; i < len && i < this.indexCards.length; i++) {
      const card = this.indexCards[i];

      try {
        const pdfPath = `${EMAIL_PATH}/${i}.pdf`;
        const exist = fs.existsSync(pdfPath);

        if (exist) {
          console.log(
            `already exits [${i}]; ${card.from} => ${card.to} : ${card.subject}`
          );
        } else {
          const data = await this.downloadPDF(card, options);
          fs.writeFileSync(pdfPath, data, "binary");

          console.log(
            `dl[${i}]; ${card.from} => ${card.to} : ${card.subject} ✔️`
          );
        }
      } catch (err) {
        console.warn(
          `dl[${i}]; ${card.from} => ${card.to} : ${card.subject} ❌`
        );
        console.error(err);
        exit(1);
      }
    }
  }

  private async downloadPDF(card: IndexCard, options: { [key: string]: any }) {
    const host = "https://foia.state.gov/";
    const pdfUrl = host + card.pdfLink;
    options.path = pdfUrl;

    return new Promise<Buffer>((resolve, reject) => {
      const req = https.request(options, (res) => {
        const data: Uint8Array[] = [];

        res.on("data", (chunk) => {
          data.push(chunk);
        });

        res.on("end", function () {
          if (res.statusCode != 200) {
            reject("Api call failed with response code " + res.statusCode);
          } else {
            const buffer = Buffer.concat(data);
            resolve(buffer);
          }
        });
      });

      req.on("error", (error) => {
        console.error(error);
      });

      req.end();
    });
  }

  private async getIndex() {
    try {
      if (fs.existsSync(INDEX_FILE)) {
        console.log(`${INDEX_FILE} found`);

        const rawIndex = fs.readFileSync(INDEX_FILE);
        this.indexCards = JSON.parse(rawIndex.toString());
      } else {
        console.log(`${INDEX_FILE} not found`);
        this.indexCards = await this.downloadIndexCards();

        fs.writeFileSync(INDEX_FILE, JSON.stringify(this.indexCards));
      }

      console.log(`index contains ${this.indexCards.length} entries`);
    } catch (err) {
      console.error(err);
      exit(1);
    }
  }

  private async downloadIndexCards() {
    const start = 0;
    const limit = 5000000;

    const options = {
      hostname: "foia.state.gov",
      port: 443,
      path: `/api/Search/SubmitSimpleQuery?searchText=*&collectionMatch=Clinton_Email&page=1&start=${start}&limit=${limit}&beginDate=false&endDate=false&postedBeginDate=false&postedEndDate=false&caseNumber=false&sort=&_=1602334804498`,
      method: "GET",
      headers: {
        Accept: "application/json, text/javascript",
        "Accept-Encoding": "gzip, deflate, br",
        Host: "foia.state.gov",
        Referer:
          "https://foia.state.gov/search/results.aspx?collection=Clinton_Email",
      },
    };

    console.log(`downloading index from ${options.hostname}`);

    return new Promise<IndexCard[]>((resolve, reject) => {
      const req = https.request(options, (res) => {
        let body = "";

        res.setEncoding("utf8");

        res.on("data", (chunk) => {
          body = body + chunk;
        });

        res.on("end", function () {
          if (res.statusCode != 200) {
            reject("Api call failed with response code " + res.statusCode);
          } else {
            const parsedIndex = JSON.parse(body.toString());
            const rawResults = parsedIndex.Results as IndexCard[];
            resolve(rawResults);
          }
        });
      });

      req.on("error", (error) => {
        console.error(error);
      });

      req.end();
    });
  }
}

const scrape = new Scrape();
scrape.scrape();
