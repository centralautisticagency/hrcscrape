import fs from "fs";
import { EMAIL_PATH, TEXT_PATH } from "./config";
import path from "path";
import PdfParse from "pdf-parse";
import { exit } from "process";

class PdfToText {
  constructor() {}

  async parse() {
    if (!fs.existsSync(TEXT_PATH)) {
      fs.mkdirSync(TEXT_PATH);
    }

    const pdfFiles = await (await this.getFolderFilenames(EMAIL_PATH)).sort();

    try {
      pdfFiles.forEach(async (pdfFileName) => {
        const textFileName = path.parse(pdfFileName).name;
        const pdfPath = path.join(__dirname, "..", EMAIL_PATH, pdfFileName);
        const textPath = path.join(
          __dirname,
          "..",
          TEXT_PATH,
          textFileName + ".txt"
        );

        if (!fs.existsSync(pdfPath) || fs.existsSync(textPath)) {
          // console.log(`pdf2txt skip [${textFileName}] as it already exists`);
          return;
        }

        const pdfData = fs.readFileSync(pdfPath);
        const parsed = await this.parsePDF(pdfData, pdfFileName);

        fs.writeFileSync(textPath, parsed, "binary");
      });
    } catch (err) {
      console.error(err);
      exit(1);
    }
  }

  async parsePDF(buffer: Buffer, pdfFileName: string) {
    return new Promise<string>((resolve, reject) => {
      PdfParse(buffer)
        .then(function (data) {
          console.log(`Parsed "${pdfFileName}" ✔️`);
          resolve(data.text);
        })
        .catch(function (error) {
          console.error(`Unable to parse "${pdfFileName}" ❌`);
          reject(error);
        });
    });
  }

  async getFolderFilenames(path: string) {
    return new Promise<string[]>((resolve, reject) => {
      fs.readdir(path, function (err, filenames) {
        if (err) {
          throw new Error(err.message);
        }

        resolve(filenames);
      });
    });
  }
}

const pdfToText = new PdfToText();
pdfToText.parse();
