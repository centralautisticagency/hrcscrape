# HRCScrape
Scraper for downloading all foia documents released 10.10.2020 Collection: Clinton_Email

### Stack
* nodejs
* typescript

### Setup
Get a node and typescript environment installed globally, then install nodemodules. If you dont know what this means then tough luck

### Usage
* `yarn run tsc` to compile
* `node build/scrape.js` to run
  * accepts arguments `start index` and `batch size` to split up the work
* will skip previously downloaded files, delete `emails/` and `index.json` if you need to re-download

* `node build/pdftotext.js` to convert files in `emails/` folder to raw text, perfect for grep searches
  * If node warns about running out of memory, use `node --max-old-space-size=4092 build/pdftotext.js` to allocate a bigger heap space